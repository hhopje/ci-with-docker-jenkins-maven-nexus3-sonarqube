# What is this 

A Continuous Integration environment with Jenkins (for maven projects) + Nexus3 + Sonarqube  in docker containers.

The Jenkins server has following plugins installed: 
bitbucket:1.1.5
maven-plugin:2.13
build-timeout:1.17.1
docker-build-publish:1.3.1
sonar:2.4.4

Furthermore, maven 3.3.9 and docker client is installed. This docker client uses the docker engine of the host on which it runs.

# How to build #
See build.sh

# How to run #

Step 1 & 2 needs to be done only once on your machine

1. First create a Jenkins volume container in order to persist the Jenkins data even after removal of the Jenkins container: see create-data-volume.sh

2. Then create a Nexus volume container: see nexus3/create-data-volume
 
3. Start the Nexus3 container, see nexus3/run.sh

4. Start the Sonarqube container, see sonarqube/run.sh

5. Finally start the Jenkins container, see run.sh.
Note: if you want to run jenkins without running "nexus3" and "sonarqube" containers then remove the "--link nexus3:nexus3 --link sonarqube:sonarqube" from the script.

After some minutes, the containers can be reached by:

http://localhost:8080 for jenkins (default credentials admin admin)

http://localhost:8081 for nexus3 (default credentials admin admin123)

http://localhost:9000 for sonarqube

# Alternatively: build and run with docker-compose #

Only once on your machine: see step 1 and 2 above.

Then: see compose-up.sh for the command to build and run jenkins + nexus3 + sonarqube in one action.

# Warning #
Use the utils/remove-* scripts with caution! Eg remove-all-volumes  as it will remove Jenkins data and probably also other persistent data.

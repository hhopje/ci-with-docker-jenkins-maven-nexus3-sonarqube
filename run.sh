docker create --name jenkins-data-container \
  -v jenkins-data-volume:/var/jenkins_home \
  hopje/jenkins /bin/true
docker run --name jenkins -d \
  -p 8080:8080 -p 50000:50000 \
  --volumes-from jenkins-data-container \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v /etc/timezone:/etc/timezone \
  -v /etc/localtime:/etc/localtime \
  --link nexus3:nexus3 \
  --link sonarqube:sonarqube \
  hopje/jenkins
docker cp ./settings.xml jenkins:/var/jenkins_home/.m2

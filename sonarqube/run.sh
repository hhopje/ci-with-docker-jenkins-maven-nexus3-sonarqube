docker run --name sonarqube \
  -d -p 9000:9000 -p 9092:9092 \
  -v /etc/timezone:/etc/timezone \
  -v /etc/localtime:/etc/localtime \
  sonarqube:5.6.3

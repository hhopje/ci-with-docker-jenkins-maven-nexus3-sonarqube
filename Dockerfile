FROM jenkins:2.7.4

# Install Jenkins Plugins 
RUN /usr/local/bin/install-plugins.sh bitbucket:1.1.5
RUN /usr/local/bin/install-plugins.sh maven-plugin:2.13
RUN /usr/local/bin/install-plugins.sh build-timeout:1.17.1
RUN /usr/local/bin/install-plugins.sh docker-build-publish:1.3.1
RUN /usr/local/bin/install-plugins.sh sonar:2.4.4

# get maven 3.3.9
USER root
ARG MAVEN_VERSION=3.3.9
RUN wget --no-verbose -O /tmp/apache-maven-${MAVEN_VERSION}.tar.gz http://archive.apache.org/dist/maven/maven-3/${MAVEN_VERSION}/binaries/apache-maven-${MAVEN_VERSION}-bin.tar.gz

# verify checksum
RUN echo "516923b3955b6035ba6b0a5b031fbd8b /tmp/apache-maven-${MAVEN_VERSION}.tar.gz" | md5sum -c

# install maven
RUN tar xzf /tmp/apache-maven-${MAVEN_VERSION}.tar.gz -C /opt/
RUN ln -s /opt/apache-maven-${MAVEN_VERSION} /opt/maven
RUN ln -s /opt/maven/bin/mvn /usr/local/bin
RUN rm -f /tmp/apache-maven-${MAVEN_VERSION}.tar.gz
ENV MAVEN_HOME /opt/maven

# Install Docker CLI only (uses Host Docker engine by run container with -v /var/run/docker.sock:/var/run/docker.sock)
RUN apt-get update && apt-get install -y apt-transport-https ca-certificates
RUN apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
RUN echo 'deb https://apt.dockerproject.org/repo debian-jessie main' >> /etc/apt/sources.list.d/docker.list
RUN apt-get update && apt-cache policy docker-engine
RUN apt-get update && apt-get install -y docker-engine

# reset user to jenkins
RUN usermod -a -G docker jenkins
USER jenkins

docker create -v nexus-data-volume:/nexus-data \
  --name nexus-data-container \
  sonatype/nexus3:3.0.2 /bin/true
docker run --name nexus3 -d \
  -p 8081:8081 \
  --volumes-from nexus-data-container \
  -v /etc/timezone:/etc/timezone \
  -v /etc/localtime:/etc/localtime \
  sonatype/nexus3:3.0.2
